public class DifficultyPack1
{
	public enum Difficulty
	{
	  EASY("Easy"), HARD("Hard"), INTERMEDIATE("Intermediate");
    
	  private String setting;
	  
	  Difficulty(String setting)
	  {
	  	this.setting = setting;
	  }

	  public String getDifficulty(){ return setting; }  
	}
}