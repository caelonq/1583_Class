public abstract class GameCharacter implements Health
{
	private String name;
	private int health = Health.HEALTH;
	
	// GameCharacter 1-Arg Constructor.
	GameCharacter(String name){ this.name = name; }

	// Returns Name Of Character.
	public String getName(){ return name; }

	// Sets Name Of Chracter.
	public void setName(String name){ this.name = name; }

  public abstract String stats();

  @Override
  public boolean alive()
  {
    return (getHealth() <= 0 ? false: true);
  }

  @Override
  public int getHealth()
  {
    return health;
  }

  @Override
  public void setHealth(int health)
  {
    this.health = health;
  }
}