public interface Combat{

 void attack(InteractiveGameCharacter character);

 int getHp();

 void combatOptions();

 void setAttack(int arg);

 void special(InteractiveGameCharacter character);

 void setSpecial(int arg);

 int getSpecial();


}