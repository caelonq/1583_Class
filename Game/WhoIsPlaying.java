import java.util.Scanner;
public class WhoIsPlaying
{
	private static Scanner input = new Scanner(System.in);
	private static InteractiveGameCharacter[] characters;
	private static LittleRed littleRedPlayer = null;
  private static LittleRed littleRedPlayer2 = null;
	private static Juggernaut minion = null;
  private static InteractiveGameCharacter character = null;
	private static int turn = 1;
	private static String m = "m"; 
	private static String s = "s";
	private static boolean monster = false;

	public WhoIsPlaying(){}


	public static void whoIsPlaying()
	{
		String name = "";
		System.out.printf("Single Player -- s\n Or\nMultiplayer -- m ?\n Enter s or m: ");
		String answer = input.nextLine();
		int numberOfPlayers = 2;

		if(answer.equalsIgnoreCase("m")){
			monster = false;
		}
		else if (answer.equalsIgnoreCase("s")){
		 	monster = true;
		}

		characters = new InteractiveGameCharacter[numberOfPlayers];

		
		//for(int player = 0; player < characters.length; ++player)
		for(int player = 0; player < numberOfPlayers; player++)
		{
			if(monster == false)
			{
				  System.out.printf("Player %d Name\n Enter: ", player + 1);
					name = input.nextLine();
					characters[player] = new LittleRed(name);
				
			}
			else 
			{
				if(player == 0)
				{
					System.out.printf("Player %d Name\n Enter: ", (player+1));
					name = input.nextLine();
					characters[0] = new LittleRed(name);
					
				}
				else if (player == 1)
				{
					characters[1] = new Juggernaut();
				}
			}

			if(monster == false)
			{
			  littleRedPlayer = (LittleRed) characters[0];
			  littleRedPlayer2 = (LittleRed) characters[1];
			}
      else
      {
        littleRedPlayer = (LittleRed) characters[0];
        minion = (Juggernaut) characters[1];
      }
		}

		characters[0].versus(characters[1]);
		characters[1].versus(characters[0]);
    character = littleRedPlayer;
	}

	public static void turnCheck()
	{
    if(monster == false)
    {
      ++turn;
			if(turn%2 == 0)
			{ 
				character = littleRedPlayer;
			}
			else
			{
				character = littleRedPlayer2;
			}
    }

		else
		{
      character = littleRedPlayer;
      ++turn;
      if(turn%6 == 0 || turn%7 == 0 || turn%3 == 0){ minion.combatOptions(); }
    }
	}

  public static LittleRed whoseTurn(){ return (LittleRed) character; }

	public static boolean isLittleRed()
	{
			boolean isLittleRed = true;
			if(!(whoseTurn() instanceof LittleRed)){ isLittleRed = false; }

			return isLittleRed;

	}

	public static void turn()
	{
      System.out.println(whoseTurn().stats());

      whoseTurn().combatOptions();
      checkIfAlive();
	}
	
  public static void checkIfAlive()
	{
		if(characters[0].alive() == false)
		{ 
			System.out.printf("%s Is Going To The Morg\n ", characters[0].getName());
			System.out.printf("%s Wins! \n ", characters[0].getVersus().getName());
			System.exit(0);
		}

		else if(characters[1].alive() == false)
		{ 
			System.out.printf("%s Is Going To The Morg\n ", characters[1].getName());
			System.out.printf("%s Wins! \n ", characters[1].getVersus().getName());
			System.exit(0);
		}
	}	
}
