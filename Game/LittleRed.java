import java.util.Scanner;

public class LittleRed extends Player
{
	private int cookies;
  private final int NEWSPAPER = 80;
  private final int KALE = 350;
  private final int TEFLON = 500;
  private int uses = 2;
  private final String a = "a";
  private static Scanner input = new Scanner(System.in);

  public LittleRed(String name)
  {
    super(name);
    this.setAttack(NEWSPAPER);
    this.setSpecial(KALE);
  }

  public void protection()
  {

    if(cookies == 6)
      { 
        System.out.printf("You're Close To Ten Cookies Take Some Extra Protection. %n");
        this.setShield(TEFLON);
      }
  }

	public void addCookie(){ ++cookies; }

	public void subtractCookie(){ --cookies; }

  public int getCookies(){ return cookies;}

	public boolean getBagOfCookies()
	{
    this.protection();
		boolean bagOfCookies = (cookies >= 10 || cookies < 0? true: false);

    if(bagOfCookies == true && cookies < 0)
    { 
        System.out.println("You've Lossed All Your Cookies !"); 
        System.exit(0);
    }
    else if(bagOfCookies == true && cookies >= 10)
      { 
        System.out.println("You Won ! . . . All Cookies Collected!");
        System.exit(0);
      }

    return bagOfCookies;
	}

  @Override
  public String stats()
  { 
    return String.format("%sCookies: %s%n", super.stats(), this.getCookies());
  }

  @Override
  public void combatOptions()
  {
  	boolean toggle = false;
    while(toggle == false)
    {
	    System.out.printf("Enter a -- to attack %s %n : ", this.getVersus().getName());
      System.out.printf("Enter k -- to attack %s %n : ", this.getVersus().getName());
  
	    String carrot = input.nextLine();

	    switch (carrot)
	    {
	      case "a":
	      case "A":
          System.out.printf("\n\nNEWSPAPER attack !\n");
	        this.attack(this.getVersus());
	        toggle = true;
	        break;
        case "k":
        case "K":
          if(uses > 0)
          {
            --uses;
            System.out.printf("\n\nKALE attack !\n");
            this.special(this.getVersus());
            toggle = true; 
          }
          else
          {
            System.out.println("Out of Kale . . .");
          }
          break;
	      default:
	        break;
	    }
	  }
  } 
}