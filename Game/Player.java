public abstract class Player extends InteractiveGameCharacter
{	
	private int shield;
 
	// Player 1-Arg Cnstructor.
	Player(String name)
	{
	  super(name);
	}
 
  public void setShield(int shield)
  {
    this.shield = shield;
    super.setHealth(this.shield + super.getHealth());
  }
}