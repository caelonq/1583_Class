public class Juggernaut extends Monster
{
	private final int HEADBUTT = 500;
  private int switchh = 1;


	Juggernaut(String name)
	{ 
    super(name);
    setAttack(HEADBUTT);
	} 

  Juggernaut (){
    this("Juggernaut");
  }

  

  @Override
	public void combatOptions()
  {
    boolean toggle = false;
    String carrot = "";
    while(toggle == false)
    {
      if (switchh%2 == 0){carrot = "a";}
      else{ carrot = "A";}

      switch (carrot)
      {
        case "a":
        case "A":
          ++switchh;
          this.attack(this.getVersus());
          toggle = true;
          break;
        default:
          break;
      }
    } 
  }
}