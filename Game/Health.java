public interface Health{

	final int HEALTH = 1000;
  
  int getHealth();

  void setHealth(int arg);

  boolean alive();

}