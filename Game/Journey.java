import java.util.Scanner;
import java.security.SecureRandom;
public class Journey
{
  // Direction Constants
  public enum Direction
  {
    NORTH (0), SOUTH (1), EAST (2), WEST (3);
    private final int direction;

    // 1-Arg Integer Constructor 
    Direction(int direction){ this.direction = direction; }

    // Gets Direction returning its corresponding (integer).
    public int getDirection(){ return this.direction; }
  }

  // Priv. enum Variable(s)
  private static Direction up = Direction.NORTH;
  private static Direction down = Direction.SOUTH;
  private static Direction right = Direction.EAST;
  private static Direction left = Direction.WEST;
 
  // Priv. Array Of Room Names
  private static String[] roomNames = {"PURGATORY"," RED ROOM1"," BLUE ROOM2"," LGREEN ROOM3",
  " LGREEN ROOM4"," BEIGE ROOM5"," BEIGE ROOM6"," BEIGE ROOM7",
  " BEIGE ROOM8"," YELLOW ROOM9"," YELLOW ROOM10"," YELLOW ROOM11",
  " YELLOW ROOM12"," GREEN ROOM13"," GREEN ROOM14","ORANGE ROOM15", "SPIKED WALL"};

  /*************************************************
  * Priv. Multi-Dimensional Array Of Linked Exits. *
  * Rows Equal Room Exits                          *
  * Columns Equal Directions With Row Numbers,     *
  * Ex. (North, South, East, West)                 *
  * 16 is an Invalid Exit.                         *
  *************************************************/ 
  private static int [][] exit = {{0,1,0,0},{0,16,9,2},{3,16,1,16},{5,2,16,4},{6,16,3,16},
  {16,3,16,6},{16,4,5,7},{16,16,6,8},{16,16,7,16},{16,16,10,1},{13,16,11,9},{14,16,12,10},
  {16,16,16,11},{15,10,14,16},{15,11,16,13},{16,14,16,16}};

  // Priv. Static Variable(s)
  private static Scanner input = new Scanner(System.in);
  private static SecureRandom randomPlace = new SecureRandom();
  private static int place = 0;
  private static String startingRoom;
  private static int nextRoom;
  private static int var = 0;
  private static int moves = 0;
  private static WhoIsPlaying who = new WhoIsPlaying();

  // Returns Row.
  public static int getPlace(int var)
  {
    if( var < 16)
    {
      startingRoom = roomNames[place];
      nextRoom = place;
      place = exit[place][var];
      ++moves;
    }
    return place;
  }

  // Displays Room Information and Character Placement.
  public static void courseInteractionAndDisplay()
  {
    who.whoseTurn().protection();
    who.whoseTurn().getBagOfCookies();
    who.checkIfAlive();

    if(place == 16)
    {
      System.out.printf("%n%n%s%n%n%n%n","OUCH! SPIKED WALL THERE !");
      who.whoseTurn().setHealth(who.whoseTurn().getHealth()-88);

      place = nextRoom;

      if(who.isLittleRed() == true)
        { 
          who.whoseTurn().subtractCookie();
          who.whoseTurn().getBagOfCookies();
        }
    }

    else if(place == 15 || place == 8)
    {
      if(who.isLittleRed() == true)
      {
        who.whoseTurn().addCookie();
        who.whoseTurn().getBagOfCookies();
      }
      int generatedPlace =  randomPlace.nextInt(15);

      if (generatedPlace == 8 || generatedPlace == 15)
        place = 9;
      else
        place = generatedPlace;
    }

    else if (place == 4 || place == 14 || place == 12 || place == 5){ who.turn(); who.turnCheck();  }

      System.out.printf("%nCurrent room: %s%n", roomNames[place]);  
      System.out.printf("%n__________________________________________%n");
    
      System.out.printf("%s Have %d Cookies %n", who.whoseTurn().getName(), who.whoseTurn().getCookies());
      System.out.printf("%n__________________________________________%n");
      System.out.printf("NORTH Room-- %s%n%nSOUTH Room-- %s%n%n",roomNames[exit[place][0]], roomNames[exit[place][1]]);
      System.out.printf("EAST Room-- %s%n%nWEST Room-- %s%n%n",roomNames[exit[place][2]], roomNames[exit[place][3]]);
      System.out.printf("%n__________________________________________%n");
  }

 // Displays Room Passage.
  public static void gameCourse_A()
  {
    System.out.printf("%n%n`Welcome To GameCourseA`%n");
    displayObjective();
    who.whoIsPlaying();
    boolean game = false;
    boolean over = true;
    boolean statsChecked = false;

    do{
      System.out.println("____________________");
      System.out.printf("%nEnter Q to Quit%n");
      System.out.printf("Enter G to check stats\n\n");  
      System.out.printf(" Enter a Direction (N,S,E,W)%n NORTH--N(^) |%n SOUTH--S(v)"); 
      System.out.printf("|%n EAST--E(>) |%n WEST--W(<) |%n");
      String grabDirection = input.nextLine();
      System.out.printf("%s%n%n", "__________________________________________");
      switch(grabDirection){
        case "n":
        case "N":
          System.out.printf("%s%n%n","YOU ARE MOVING NORTH");
          getPlace(up.getDirection());
        break;
        case "s":
        case "S":
          System.out.printf("%s%n%n","YOU ARE MOVING SOUTH");
          getPlace(down.getDirection());
        break;
        case "e":
        case "E":
          System.out.printf("%s%n%n","YOU ARE MOVING EAST");
          getPlace(right.getDirection());
        break;
        case "w":
        case "W":
          System.out.printf("%s%n%n","YOU ARE MOVING WEST");
          getPlace(left.getDirection());
        break;
        case "g":
        case "G":
          System.out.println(who.whoseTurn().stats());
          System.out.println(who.whoseTurn().getVersus().stats());
          statsChecked = true;
          break;
        case "q":
        case "Q":
          game = over;
          System.out.printf("%nEnding Stroll%n%n");
          System.exit(0);
        default:
          System.out.printf("%s%n%n","Undefined Entry%n%n");
        break;
      }
      if(statsChecked == false)
      {
        courseInteractionAndDisplay();
      }
        statsChecked = false;
    }while(game != over);
  }

  // Displays objective.
  private static void displayObjective()
  {
    System.out.printf("Objective 1 ~ Teamwork%n- ");
    System.out.printf("You End Your Turn, thus, Be Courteous To Your Opponent%n");
    System.out.printf("Objective 2 ~ Got Milk?%n- "); 
    System.out.printf("Grab The Cookies Hidden In ORANGE15 room and BEIGE8 room.%n");
    System.out.printf("Objective 3 ~ SPIKES%n- "); 
    System.out.printf("Health is lost if you run into a SPIKED WALL! Avoid The SPIKES.!%n");
    System.out.printf("Objective 4 ~ Enjoy The Game%n___________%n");
    System.out.printf("NOTE: enter done after every selection.%n___________%n");
      System.out.printf("%nCurrent room: %s%n", roomNames[place]);  
      System.out.printf("%n__________________________________________%n");
      System.out.printf("NORTH Room-- %s%n%nSOUTH Room-- %s%n%n",roomNames[exit[place][0]], roomNames[exit[place][1]]);
      System.out.printf("EAST Room-- %s%n%nWEST Room-- %s%n%n",roomNames[exit[place][2]], roomNames[exit[place][3]]);
      System.out.printf("%n__________________________________________%n");

  }
}// End Class
