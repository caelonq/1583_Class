public abstract class InteractiveGameCharacter extends GameCharacter implements Combat
{
	private int attack;
  private int special;
	private InteractiveGameCharacter character;

  public InteractiveGameCharacter(String name){ super(name); }

  public void versus(InteractiveGameCharacter character)
  {
    this.character = character;
  }

  public InteractiveGameCharacter getVersus(){ return character;}

   @Override
  public int getHp()
  {
    return attack;
  }

  @Override
	public void attack(InteractiveGameCharacter interactiveGameCharacter)
	{ 
	  interactiveGameCharacter.setHealth(interactiveGameCharacter.getHealth()-this.attack);
	}

  @Override
  public void setAttack(int attack)
  {
    this.attack = attack;
  }

  @Override
  public int getSpecial()
  {
    return special;
  }

  @Override
  public void setSpecial(int special)
  {
    this.special = special;
  }

   @Override
  public void special(InteractiveGameCharacter interactiveGameCharacter)
  { 
    interactiveGameCharacter.setHealth(interactiveGameCharacter.getHealth()-this.special);
  }

	// public void pickUp(Flowers flower);

  @Override
	public String stats()
  { 
    return String.format("%s: %s%n%nHealth: %s%nHp: %s%nSpecial Hp: %s\n", getClass().getName(), super.getName(),
     super.getHealth(), getHp(), getSpecial());
  } 

  @Override
  public void combatOptions()
  {
  	boolean toggle = false;
    while(toggle == false)
    {
	    System.out.printf("a -- Attack%n Enter: ");

	    String carrot = "";

	    switch (carrot)
	    {
	      case "a":
	      case "A":
	        this.attack(character);
	        toggle = true;
	        break;
	      default:
	        break;
	    }
	  }
  } 

}

