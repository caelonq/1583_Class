import java.util.Scanner;

public class GamePackage
{
  // Priv. Static Instance Variable(s).
  private static Player player1;
  private static Player player2;
  private static Monster monster;
  private static String playerNameInput;
  private Scanner input = new Scanner(System.in);
  private static String playerSettings;
  private static String playerCombatRequest;
  private static boolean done = false;
  private static boolean gameEnd = false;
  private static DifficultyPack1 difficil = new DifficultyPack1();
  private static GameCourseA gameCourseA = new GameCourseA();
  private static String gameEnding = "Start";
  private static int turn = 2;
  private static int attacking;
  private static boolean playerChoice = false;
  private static int numberOfPlayers;

  // No-Arg Constructor.
  GamePackage(){}

  // Returns Game's Difficulty.
  public DifficultyPack getGameDifficulty(){ return difficil.gameDifficulty(); }


    /********************************
    * public static int sleep()     *
    * --sleep method for monster--  *
    * return true;                  *
    *********************************/

  // Starts Game.
  public void startGameCourse()throws InterruptedException
  {
    playerRequestPrompt();
    do{
        if(numberOfPlayers == 2)
        {
          if(turn%2 == 0 && player1.getBag() >= -1 || player1.getWins() <= 0)
          {
            player1.gameCourse();
            playerCombat();
          } 
          else if(player2.getBag() >= -1 && player2.getWins() <= 0)
          {
          player2.gameCourse();
          playerCombat();
          }
        }

        else
        {
            if(turn%2 == 0 && player1.getBag() >= -1 || player1.getWins() <= 0)
            {
              player1.gameCourse();
              playerCombat();
            }
            else if (player1.getBag() >= -1 && player1.getWins() <= 0)
            {
              playerCombat();
            }
           else
             gameEnding = "END";
        }

      }while(getEnding() != "END");
  }

  // One Player Constructor
  public void onePlayer()
  {
    numberOfPlayers = 1;
    System.out.printf("%nPlayer 1 Name: ");
    playerNameInput = input.nextLine();
    player1 = new Player(playerNameInput);
    customizeOrDefault(player1);
    System.out.printf("%n NAME YOUR MONSTER ! :");
    playerNameInput = input.nextLine();
    monster = new Monster(playerNameInput);
    monster.defaultSettings();
  }

  // Two Player Consttructor.
  public void twoPlayers()
  {
    numberOfPlayers = 2;
    System.out.printf("%nPlayer 1 Names: ");
    playerNameInput = input.nextLine();
    player1 = new Player(playerNameInput);
    customizeOrDefault(player1);
    System.out.printf("%nPlayer 2 Name: ");
    playerNameInput = input.nextLine();
    player2 = new Player(playerNameInput);
    customizeOrDefault(player2);
  }

  // Customize Or Default Prompt.
  public void customizeOrDefault(Player player)
  {
    done = false;
    gameEnd = false;
    while(done == false)
    {
      System.out.println("____________________");
      System.out.printf("%nEnter Q to Quit%n%n");
      System.out.printf("Type Yes to Customize Player || Type No to Use Default Settings%n%n%s %s or %s%n%n<%s>%n______________________%n%s Enter: ",
      "Enter Answer","Yes","NO", "To Confirm--Type Done", player.getName());
      playerSettings = input.nextLine();
      switch(playerSettings)
      {
        case "yes":
        case "Yes":
          player.customize();
          done = true;
          break;
        case "no":
        case "No":
          player.defaultSettings();
          done = true;
          break;  
        case "done":
        case "Done":
          done = true;
          break;
        case "Q":
        case "q":
          System.exit(0);
          break;   
        default:
          System.out.printf("%n%nInvalid Entry%n%n");
          break;
      }
    }
  }

  // Player Request Prompt.
  public void playerRequestPrompt()
  {
    while(done != true)
    {
      System.out.println("____________________");
      System.out.printf("%nEnter Q to Quit%n%n");
      System.out.println("To Confirm-- Type Done");
      System.out.printf("%n%nOne Player?--Type One. Two Players?--Type Two.%n%n%n______________________%n Enter: ");
      playerSettings = input.nextLine();
      switch(playerSettings)
      {
        case "one":
        case "One":
          onePlayer();
          break;
        case "two":
        case "Two":
          twoPlayers();
          break; 
        case "done":
        case "Done":
          done = true;
          break;
        case "q":
        case "Q":
          System.exit(0);
          break;
        default:
          System.out.printf("%n%nInvalid Entry%n%n");
          break;
      }
    }
  }

  // Game Combat Method. 
  public void playerCombat()throws InterruptedException
  {
    if(numberOfPlayers == 1)
    {
      if(turn%2 == 0 && player1.getWins() < 1)
      {
        player1CombatPrompt(monster);
        ++turn;
      }
      else if (player1.getWins() < 1)
      {
        monsterCombatPrompt(player1);
        ++turn;
      }
        player1.getStats();
        monster.getStats();
    }
    else if(numberOfPlayers == 2 && (player1.getBag() >= -1 || player2.getBag() >= -1))
    {
      if(turn%2 == 0 )
      {
        player1CombatPrompt(player2);
        ++turn;
      }
      else
      {
        player2CombatPrompt(player1);
        ++turn;
      }
        player1.getStats();
        player2.getStats();
    }
   
  }

  // Method to Determine Wins and Losses.
  public String getEnding()
  {
    if (numberOfPlayers == 2)
    {
      if((player1 != null && player1.getCharacterHealth() <= 0) 
        || player1.getBag() <= -2 || player2.getWins() >= 1)
      {
        System.out.printf("%n%n!!Player 2 Wins !!%n%n");
        player2.getStats();
        if (player1.getCharacterHealth() < 0){ System.out.printf("%n!!BRUTALITY !!%n%n"); }
        System.out.println("__________________");
        player1.getStats();
        gameEnding="END";
      }
      else if ((player2 != null && player2.getCharacterHealth() <= 0 )
         || player2.getBag() <= -2 || player1.getWins() >= 1)
      {
          System.out.printf("%n%n!!Player 1 Wins !!%n%n");
          player1.getStats();
          if (player2.getCharacterHealth() < 0){ System.out.printf("%n%n!!BRUTALITY !!%n%n"); }
          System.out.println("__________________");
          player2.getStats();
          gameEnding="END";
      }
    }else
    {
      if(monster != null && monster.getCharacterHealth() <= 0 || player1.getWins() >= 1)
      {
        System.out.printf("%n%n!!Player 1 Wins !!%n%n");
        if (monster.getCharacterHealth() < 0) { System.out.printf("%n%n!!BRUTALITY !!%n%n"); }
        player1.getStats();
        System.out.println("__________________");
        monster.getStats();
        gameEnding="END";
      }
      else if((player1 != null && player1.getCharacterHealth() <= 0) || player1.getBag() <= -2)
      {
        System.out.printf("%n%n!! Monster Wins !!%n%n");
        if (player1.getCharacterHealth() < 0){ System.out.printf("%n%n!!BRUTALITY !!%n%n"); }
        player1.getStats();
        System.out.println("__________________");
        monster.getStats();
        gameEnding="END";
      }
    }
      return gameEnding;
  }

  // Player 1's Combat Prompt.
  public void player1CombatPrompt(Character player)
  {
    playerChoice = false;
    if (player1.getCharacterHealth() <= 0 || player.getBag() <= -2 || player1.getWins() >= 1)
    {
      playerChoice = true;
    }
    while(playerChoice != true)
    {
      System.out.printf("%s Turn to Attack%n", player1.getName());
      System.out.println("_________________________");
      if (numberOfPlayers == 2){ System.out.printf( "Attacking %s %n", player2.getName()); }
      System.out.println("____________________");
      System.out.printf("%nEnter Q to Quit%n%n");
      player1.getStats();
      System.out.printf("%n%nTo Attack --Type attack///%nTo Use ThunderBolt --Type bolt%nEnter: ");
      playerCombatRequest = input.nextLine();
      switch(playerCombatRequest)
      {
        case "attack":
        case "Attack":
          player1.attack(player);
          playerChoice = true;
          break;
        case "bolt":
        case "Bolt":
          player1.thunderBolt(player);
          playerChoice = true;
          break;
        case "q":
        case "Q":
          System.exit(0);
        default:
        System.out.printf("%n%nInvalid Entry%n%n");
        break;
      }
    }
  }

  // Player 2's Combat Prompt.
  public void player2CombatPrompt(Player player)
  {
    playerChoice = false;
    if( player2.getCharacterHealth() <= 0 || player.getBag() <= -2 
      || player2.getWins() >= 1 || player1.getWins() >= 1)
    {
      playerChoice = true;
      player1.addLosses();
    }
    System.out.printf("%s Turn to Attack%n", player2.getName());
    System.out.println("_________________________");
    System.out.printf("Attacking %s%n", player1.getName());
    while(playerChoice != true)
    {
      System.out.println("____________________");
      System.out.printf("%nEnter Q to Quit%n%n");
      player2.getStats();
      System.out.printf("%n%nTo Attack --Type attack///%nTo Use ThunderBolt --Type bolt%nEnter: ");
      playerCombatRequest = input.nextLine();
      switch(playerCombatRequest)
      {
        case "attack":
        case "Attack":
          player2.attack(player);
          playerChoice = true;
          break;
        case "bolt":
        case "Bolt":
          player2.thunderBolt(player);
          playerChoice = true;
          break;
        case "q":
        case "Q":
          System.exit(0);
          break;
        default:
          System.out.println("%n%nInvalid Entry%n%N");
          break;
      }
    }  
  }

  // Monster AkA (CPU) Combat Prompt.
  public void monsterCombatPrompt(Player player)throws InterruptedException
  {
    monster.getStats();
    System.out.println("_________________________");
    System.out.printf("%n%n%s Attacking %s%n%n%n", monster.getName(), player1.getName());
    playerChoice = false;
    ++attacking;
    if (monster.getCharacterHealth() <= 0 || player1.getWins() >= 1)
    {
      playerChoice = true;
      attacking = 0;
      player1.addLosses();
      }
      while(attacking > 0 && playerChoice == false)
      {
        Thread.sleep(4000);
        switch(attacking)
        {
          case 1:
            monster.attack(player);
            playerChoice = true;
            ++attacking;
            playerChoice = true;
            break;
          case 2:
            monster.thunderBolt(player);
            playerChoice = true;
            ++attacking;
            playerChoice = true;
            break;
          default:
            attacking = 0;
            playerChoice = true;
            break;
        }
      }
    }
  }