import java.util.Scanner;

public class GameCourseA
{
  // Direction Constants
  public enum Direction
  {
    NORTH (0), SOUTH (1), EAST (2), WEST (3);
    private final int direction;

    // 1-Arg Integer Constructor 
    Direction(int direction){ this.direction = direction; }

    // Gets Direction returning its corresponding (integer).
    public int getDirection(){ return this.direction; }
  }

  // Priv. enum Variable(s)
  private static Direction up = Direction.NORTH;
  private static Direction down = Direction.SOUTH;
  private static Direction right = Direction.EAST;
  private static Direction left = Direction.WEST;
 
  // Priv. Array Of Room Names
  private static String[] roomNames = {"PURGATORY"," RED ROOM1"," BLUE ROOM2"," LGREEN ROOM3",
  " LGREEN ROOM4"," BEIGE ROOM5"," BEIGE ROOM6"," BEIGE ROOM7",
  " BEIGE ROOM8"," YELLOW ROOM9"," YELLOW ROOM10"," YELLOW ROOM11",
  " YELLOW ROOM12"," GREEN ROOM13"," GREEN ROOM14","ORANGE ROOM15", "SPIKED WALL"};

  /*************************************************
  * Priv. Multi-Dimensional Array Of Linked Exits. *
  * Rows Equal Room Exits                          *
  * Columns Equal Directions With Row Numbers,     *
  * Ex. (North, South, East, West)                 *
  * 16 is an Invalid Exit.                         *
  *************************************************/ 
  private static int [][] exit = {{0,1,0,0},{0,16,9,2},{3,16,1,16},{5,2,16,4},{6,16,3,16},
  {16,3,16,6},{16,4,5,7},{16,16,6,8},{16,16,7,16},{16,16,10,1},{13,16,11,9},{14,16,12,10},
  {16,16,16,11},{15,10,14,16},{15,11,16,13},{16,14,16,16}};

  // Priv. Static Variable(s)
  private static Scanner input = new Scanner(System.in);
  private static int place = 0;
  private static String startingRoom;
  private static int nextRoom;
  private static int var = 0;
  private Character player;

  // Returns Row.
  public static int getPlace(int var)
  {
    if( var < 16)
    {
      startingRoom = roomNames[place];
      nextRoom = place;
      place = exit[place][var];
    }
    return place;
  }

  // Displays Room Information and Character Placement.
  public static void displayRooom(Character player)
  {
    if(place == 16)
    {
      System.out.printf("%n%n%s%n%n%n%n","OUCH! SPIKED WALL THERE !");
      player.getHealthPack().subtractHealth();
      place = nextRoom;
      player.subtractCookies();
    }
    else if(place == 15 || place == 8)
    {
      player.addCookies();
      if(player.getBag() >= 10)
      {
        player.addWins();
      }
    }

      System.out.printf("%nCurrent room: %s%n", roomNames[place]);  
      System.out.printf("%n__________________________________________%n");
      System.out.printf("%s Have %d Cookies %n", player.getName(), player.getBag());
      System.out.printf("%n__________________________________________%n");
      System.out.printf("NORTH Room-- %s%n%nSOUTH Room-- %s%n%n",roomNames[exit[place][0]], roomNames[exit[place][1]]);
      System.out.printf("EAST Room-- %s%n%nWEST Room-- %s%n%n",roomNames[exit[place][2]], roomNames[exit[place][3]]);
      System.out.printf("%n__________________________________________%n");
  }

 // Displays Room Passage.
  public static void gameCourse_A(Character player)
  {
    System.out.printf("%n%n`Welcome To GameCourseA`%n%n");
    displayObjective();
    player.getDifficultyPack().displayDifficulty1();
    boolean game = false;
    boolean over = true;

    do{
      System.out.println("____________________");
      System.out.printf("%nEnter Q to Quit%n"); 
      displayRooom(player);
      System.out.printf(" Enter a Direction (N,S,E,W)%n NORTH--N(^) |%n SOUTH--S(v)"); 
      System.out.printf("|%n EAST--E(>) |%n WEST--W(<) |%nEnds Turn(Done)%n");
      String grabDirection = input.nextLine();
      System.out.printf("%s%n%n", "__________________________________________");
      switch(grabDirection){
        case "n":
        case "N":
          System.out.printf("%s%n%n","YOU ARE MOVING NORTH");
          getPlace(up.getDirection());
        break;
        case "s":
        case "S":
          System.out.printf("%s%n%n","YOU ARE MOVING SOUTH");
          getPlace(down.getDirection());
        break;
        case "e":
        case "E":
          System.out.printf("%s%n%n","YOU ARE MOVING EAST");
          getPlace(right.getDirection());
        break;
        case "w":
        case "W":
          System.out.printf("%s%n%n","YOU ARE MOVING WEST");
          getPlace(left.getDirection());
        break;
        case "done":
        case "Done":
          game = over;
          System.out.printf("%nEnding Stroll%n%n");
        break;
        case "q":
        case "Q":
          System.exit(0);
        default:
          System.out.printf("%s%n%n","Undefined Entry%n%n");
        break;
      }
    }while(game != over);
  }

  // Displays objective.
  private static void displayObjective()
  {
    System.out.printf("Objective 1 ~ Teamwork%n- ");
     System.out.printf("You End Your Turn, thus, Be Courteous To Your Opponent%n");
     System.out.printf("Objective 2 ~ Got Milk?%n- "); 
     System.out.printf("Grab The Cookies Hidden In ORANGE15 room and BEIGE8 room.%n");
     System.out.printf("Objective 3 ~ SPIKES%n- "); 
     System.out.printf("Health is lost if you run into a SPIKED WALL! Avoid The SPIKES.!%n");
     System.out.printf("Objective 4 ~ Enjoy The Game%n___________%n");
     System.out.printf("NOTE: enter done after every selection.%n___________%n");
  }
}// End Class