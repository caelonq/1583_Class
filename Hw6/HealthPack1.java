public class HealthPack1{

DifficultyPack difficulty;
private int health = 1000;

  HealthPack1(){
    this.health = health;
  }

  public int getHealth(){
      return this.health;
  }
  public void setHealth(int salud){
    this.health = salud;
  }
  public void customSetHealth(){ this.health = 1000; }

  public void addHealth(){
    if(difficulty == DifficultyPack.hard){
      this.health += 7;
    }
    else if(difficulty == DifficultyPack.easy){
      this.health += 16;
    }
    else if(difficulty == DifficultyPack.intermediate){
      this.health += 12;
    }
  }

  public void healthSettings(DifficultyPack difficulty){
      this.difficulty = difficulty;
  }

  public void subtractHealth(){
    if(difficulty == DifficultyPack.hard){
      this.health -= 550;
    }
    else if(difficulty == DifficultyPack.easy){
      this.health -= 350;
    }
    else if(difficulty == DifficultyPack.intermediate){
      this.health -= 450;
    }
  }

}

class CombatPack1{

DifficultyPack difficulty;
private int attack = 0;
private int specialty = 0;
private int bolt = 0;

  CombatPack1(){
    this.attack = attack;
  }

  public int attack(){
      return this.attack;
  }

  public void setAttack(int newAttack){
    this.attack = newAttack;
  }
  public void setSpecialty(int newSpecialty){
    this.specialty = newSpecialty;
  }

  public void subtractSpecialty(){ this.specialty--;}

  public int specialty(){
    return this.specialty;
  }
  public int bolt(){
    return this.bolt;
  }
  public int thunderBolt(){
    if(this.specialty != 0){
      this.bolt = this.attack + this.specialty;
    } return this.bolt;
  }
  public void customSetCombat()
  { 
    this.specialty = 0;
    this.attack = 0;
    this.bolt = 0; 
  }
  public void addChi(){
    if(difficulty == DifficultyPack.hard){
      this.specialty += 30;
      this.bolt = this.specialty + this.attack;
    }
    else if(difficulty == DifficultyPack.easy){
      this.specialty += 56;
      this.bolt = this.specialty + this.attack;
    }
    else if(difficulty == DifficultyPack.intermediate){
      this.specialty += 43;
      this.bolt = this.specialty + this.attack;
    }
  }

  public void combatSettings(DifficultyPack difficulty){
    this.difficulty = difficulty;
  }

  public void defaultCombatSettings(){
    if(difficulty == DifficultyPack.hard){
      this.attack = 64;
      this.specialty = 47;
      this.bolt = 0;
    } 
    else if(difficulty == DifficultyPack.easy){
      this.attack = 89;
      this.specialty = 44;
      this.bolt = 0;
    }
    else if(difficulty == DifficultyPack.intermediate){
      this.attack = 76;
      this.specialty = 49;
      this.bolt = 0;
    }
  }
}