import java.util.Scanner;

public class Character
{
  // Private Instance Variable(s).
  private String name;
  private HealthPack1 health = new HealthPack1();
  private CombatPack1 combat = new CombatPack1();
  private CustomPackage1 custom = new CustomPackage1();
  private DifficultyPack1 difficulty = new DifficultyPack1();
  private GameCourseA gameCourseA = new GameCourseA();
  private int bag = 0;
  private int wins = 0;
  private int losses = 0;

  // Character Constructor.
  public Character(String name){ this.name = name; } 

  // Game Course Method With The Course's GameCourseA Class Implementation
  public void gameCourse(){ gameCourseA.gameCourse_A(this); }

  // Setting(s) Method.
  public void chooseDifficulty(){ difficulty.changeDifficulty(this); }
  public void defaultSettings(){ difficulty.defaultDifficulty(this); }
  public void customSettings()
  {
    this.combat.customSetCombat();
    this.health.customSetHealth();
  }

  /*********************************************************
  * Customize Character Method.
  * Changes Character Atributes To Custom Settings.
  * Sets Difficulty.
  * Uploads Menu From Custom Package To Customize Character.
  **********************************************************/
  public void customize()
    {
      customSettings();
      custom.setcustomDifficulty(setCharacterDifficulty(), this);
      custom.customMenu(this);
    }

  // Returns A String Representation Of Character's (name).
  public String toString(){ return String.format("Character %s", this.name); }

  // Increase Health Method -- Implemented When Customizing Character.
  public void raiseShield()
  {
    do
    {
      this.health.addHealth();
      this.combat.subtractSpecialty();
    }while(this.combat.specialty() > 0);
  }

  // Combat Methods.
  public void attack(Character player){ player.health.setHealth(getCharacterHealth() - this.getAttack()); }
  public void thunderBolt(Character player){ player.health.setHealth(getCharacterHealth() - this.getThunderBolt()); }

  // GETTERS !
  public DifficultyPack1 getDifficultyPack(){ return this.difficulty; }
  public int getAttack(){ return this.combat.attack(); }
  public int getChi(){ return this.combat.specialty(); }
  public int getBolt() { return this.combat.bolt(); }
  public int getThunderBolt(){ return this.combat.thunderBolt();}
  public HealthPack1 getHealthPack(){ return this.health; }
  public CombatPack1 getCombatPack(){ return this.combat; }
  public CustomPackage1 getCustomPackage(){ return this.custom; }
  public int getCharacterHealth(){ return this.health.getHealth(); }
  public int getBag(){ return this.bag; }
  public int getWins(){ return this.wins; }
  public String getName(){ return this.name; }
  public DifficultyPack getDifficulty(){ return difficulty.getDifficulty1(); }
  public void getStats()
  {
    System.out.printf(">%s<%n_____________%n%nHealth : %d%n", this.name, this.getCharacterHealth());
    System.out.printf("Attack : %d%n Chi : %d%n ThunderBolt", this.getAttack(), this.getChi()); 
    System.out.printf(":%d%n Cookies %d%n_______________%n",this.getBolt(), this.getBag());
  }

  // Add and Subtract Methods. -- wins/losses/coookies
  public void addWins(){ ++wins; }
  public void addCookies(){ ++bag; }
  public void subtractCookies(){ --bag; }
  public void addLosses(){ ++losses; }

  // SETTERS !
  public DifficultyPack setCharacterDifficulty(){ return this.difficulty.changeDifficulty(this); }
  public void setCharacterGameDifficulty(DifficultyPack setting){ difficulty.setGameDifficulty(this,setting);}

}
