import java.util.Scanner;

/*****************************************************************************
* Long Term. . . Concluded A Class Would Be More Efficient Than An Enum.  *
******************************************************************************/

public class Difficulty
{

  // Priv. Instance Variable(s)
  private DifficultyPack difficulty; 
  private static String setting;
  private String input;

  // No-Arg Constructor
  Difficulty(){}

  // Sets Difficulty To A Difficulty Package Object and Sets its Difficulty Information.
  public void setDifficulty(DifficultyPack sett)
  {
    setting = sett.setting;
    this.difficulty = sett;
  }

  // Displays Difficulty.
  public void displayDifficulty(){ System.out.printf("%s ", toString()); }

  // Returns Difficulty
  public DifficultyPack getDifficulty(){ return difficulty; }

  // toString Method For Difficulty's Setting.
  public String toString(){ return String.format("%nGame Difficulty: %s%n", setting); }
}

class DifficultyPack
{

  // Public Constant Setting Variable.
  public final String setting;

  // 1-Arg String Contructor.
  public DifficultyPack(String name){ this.setting = name; }

  // Returns Difficulty Pack Object Setting.
  public String settings(){ return this.setting; }

  // public static Difficulty Objects.
  public static DifficultyPack hard = new DifficultyPack("hard");
  public static DifficultyPack easy = new DifficultyPack("easy");
  public static DifficultyPack intermediate = new DifficultyPack("intermediate");
}

class DifficultyPack1
{

  // Priv. Static Variable(s)
  public static Difficulty difficulty = new Difficulty();
  private static Scanner input = new Scanner(System.in);
  private static Character player;
  private static String inputt;
  private boolean notdone = false;
  private static boolean diffSet = false;
  private static DifficultyPack dif;
  private static DifficultyPack gameDif;

  // Returns Difficulty From a Difficulty Package.
  public DifficultyPack getDifficulty1(){ return difficulty.getDifficulty(); }

  // Displays Difficulty
  public void displayDifficulty1(){ this.difficulty.displayDifficulty(); }

  // Sets Character && Character Packs Health/Combat/Custom To A Default Difficulty.
  public void defaultDifficulty(Character player)
  {
    this.player=player;
    player.getHealthPack().healthSettings(DifficultyPack.intermediate);
    player.getCombatPack().combatSettings(DifficultyPack.intermediate);
    difficulty.setDifficulty(DifficultyPack.intermediate);
    player.getCombatPack().defaultCombatSettings();
    player.getCustomPackage().setcustomDifficulty(DifficultyPack.intermediate);
  }

  // Sets Character && Character Packs Health/Combat/Custom To A Difficulty.
  public void setGameDifficulty(Character player, DifficultyPack difficultySet)
  {
    this.player = player;
    System.out.println(gameDif.settings());
    player.getHealthPack().healthSettings(difficultySet);
    player.getCombatPack().combatSettings(difficultySet);
    difficulty.setDifficulty(difficultySet);
    player.getCombatPack().defaultCombatSettings();
    player.getCustomPackage().setcustomDifficulty(difficultySet);
  }

  // Returns && Sets Character && Character Packs Health/Combat/Custom Difficulty. Menu--
  public  DifficultyPack changeDifficulty(Character player){
    this.player=player;
    while(notdone == false)
    { 
      System.out.println("____________________");
      System.out.printf("%nEnter Q to Quit%n");
      System.out.printf("%n%s%n%s%n%s%n%s%n%n<%s>%n______________________%n%s Enter: ",
      "Enter difficulty:","hard","easy","intermediate", "done", this.player.getName());
      inputt = input.nextLine();
      switch(inputt)
      {
        case "hard":
          this.player.getHealthPack().healthSettings(DifficultyPack.hard);
          difficulty.setDifficulty(DifficultyPack.hard);
          player.getCombatPack().combatSettings(DifficultyPack.hard);
          player.getCombatPack().defaultCombatSettings();
          player.getCustomPackage().setcustomDifficulty(DifficultyPack.hard);
          this.dif = DifficultyPack.hard;
          break;
        case "easy":
          player.getHealthPack().healthSettings(DifficultyPack.easy);
          difficulty.setDifficulty(DifficultyPack.easy);
          player.getCombatPack().combatSettings(DifficultyPack.easy);
          player.getCombatPack().defaultCombatSettings();
          player.getCustomPackage().setcustomDifficulty(DifficultyPack.easy);
          this.dif = DifficultyPack.easy;
          break;
        case "intermediate":
          player.getHealthPack().healthSettings(DifficultyPack.intermediate);
          difficulty.setDifficulty(DifficultyPack.intermediate);
          player.getCombatPack().combatSettings(DifficultyPack.intermediate);
          player.getCombatPack().defaultCombatSettings();
          player.getCustomPackage().setcustomDifficulty(DifficultyPack.intermediate);
          this.dif = DifficultyPack.intermediate;
          break;  
        case "done":
          notdone = true;
          break;
        case "q":
        case "Q":
          System.exit(0);
        default:
          System.out.println("Invalid Entry");
          break;
      }
    }return this.dif;
  }

  // Sets && Returns Game Difficulty. --Menu
  public static DifficultyPack gameDifficulty(){
    do{
        diffSet = false;
        System.out.println("_______________________________________________________");
        System.out.printf("%nEnter Q to Quit%n%n");
        System.out.printf("%nHard%nEasy%nIntermediate%n<To Confirm Type--Done>%n");
        System.out.printf("Enter Difficulty:%n______________________%nEnter:");
        inputt = input.nextLine();
        switch(inputt)
        {
          case "hard":
            gameDif = DifficultyPack.hard;
            break;
          case "easy":
            gameDif = DifficultyPack.easy;
            break;
          case "intermediate":
            gameDif = DifficultyPack.intermediate;
            break;
          case "done":
            diffSet=true;
            break;
          case "q":
          case "Q":
            System.exit(0);     
          default:
            System.out.println("Invalid Entry");
            break;
        }
      }while(diffSet == false);
      return gameDif;
  }
}