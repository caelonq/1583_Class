import java.util.Scanner;

public class CustomPackage1
{
  // Priv. Instance Variable(s).
  private int pizzaslices;
  private DifficultyPack difficulty;
  private String customAbilities;
  private int customSliceAllocation;
  private int toggle;
  private boolean shieldOn = false;
  private boolean not = false;
  private Character player;
  private static Scanner input = new Scanner(System.in);

  // No-Arg Constructor.
  CustomPackage1(){}

  // Method To Customize Player's Difficulty Settings.
  public void setcustomDifficulty(DifficultyPack difficulty, Character player)
  {
    this.difficulty=difficulty;
    if(difficulty==DifficultyPack.hard)
    {
      this.pizzaslices=200;
      player.getHealthPack().healthSettings(DifficultyPack.hard);
      player.getCombatPack().combatSettings(DifficultyPack.hard);
      player.customSettings();
    }
    else if(difficulty==DifficultyPack.easy)
    {
      this.pizzaslices=180;
      player.getHealthPack().healthSettings(DifficultyPack.easy);
      player.getCombatPack().combatSettings(DifficultyPack.easy);
      player.customSettings();
    }
    else if(difficulty==DifficultyPack.intermediate)
    {
      this.pizzaslices=100;
      player.getHealthPack().healthSettings(DifficultyPack.intermediate);
      player.getCombatPack().combatSettings(DifficultyPack.intermediate);
      player.customSettings();
    }
  }

    public void setcustomDifficulty(DifficultyPack difficulty)
    {
      this.difficulty = difficulty;

      if (difficulty == DifficultyPack.hard){ this.pizzaslices = 200; }

      else if(difficulty == DifficultyPack.easy){ this.pizzaslices = 180; }

      else if(difficulty == DifficultyPack.intermediate){ this.pizzaslices = 100; }
    }

  public void customMenu(Character player)
  {
    System.out.println("____________________");
    System.out.printf("%nEnter Q to Quit%n%n");
    System.out.printf("Customization Menu%n_______________________%n%n Customize Health?");
    System.out.printf("type vest%n Customize chi? type chi%nCustomize attack? type attack%n");
    System.out.printf("%n%n_______________________%n");
    while(not == false && (pizzaslices > 0 || shieldOn == false ))
    {
      player.getStats();  
      displayAbilities(player);
      System.out.printf("%nPizzaslices: %d %n_______________________%n", pizzaslices);
      System.out.printf("%n%nChi || Attack || Vest, Done? Type done after selection:");
      customAbilities = input.next();
      switch(customAbilities)
      {
        case "chi":
        case "Chi"://.toUpperCase():
            customChi(player);
            break;
        case "attack"://attack:
        case "Attack"://attack.toUpperCase():
            customAttack(player);
            break;
        case "vest"://custom:
        case "Vest"://custom.toUpperCase():
            customVest(player);
          break;
        case "done"://q:
        case "Done"://q.toUpperCase():
          System.out.printf("%n%nPizzaslices left: %d%n%n", pizzaslices);
          not = true; 
          break;
        case "q":
        case "Q":
          System.exit(0);
        default:
          System.out.println("Invalid entry");
          break;
      }
    }
      System.out.printf("%n%nPizzaslices left: %d%n%n", pizzaslices);
  }

  public void displayAbilities(Character player)
  {
    System.out.printf("%n%nAttack Power: %d%n", player.getAttack());
    System.out.printf("Chi Power: %d%nBolt Build: %d%n%n",player.getChi(), player.getBolt());
  }

  public void customChi(Character player)
  {
    if(pizzaslices == 200 || pizzaslices == 180 || pizzaslices == 100)
    {
      player.customSettings();
    }
    //////////////////////////////////////////////////////////
    if(!(pizzaslices-player.getChi()<1))
    {
      if(shieldOn == false)
      {
        player.getCombatPack().addChi();
        System.out.printf("%n pizzaslices - %d%n", player.getChi()); 
        pizzaslices -= player.getChi();
      }else
          System.out.printf("%n%n!Out of Chi! Vest Used !%n%n");
    }
    /////////////////////////////////////////////////////////////
    else if (shieldOn == false)
    {
      System.out.printf("%n%nPizzaslices Too Low ! Vest Available !");
    }else
        System.out.printf("%n%nPizzaslices Too Low ! Vest Used !");
  }

  public void customAttack(Character player)
  {
    if(pizzaslices == 200 || pizzaslices == 180 || pizzaslices == 100)
    {
      player.customSettings();
    }
    ///////////////////////////////////////////////////////////////////
    if(pizzaslices > 0 && player.getAttack() >= 0)
    {
      System.out.printf("%n%nHow much to to increase attack?%nEnter:","");
      customSliceAllocation = input.nextInt();
        if(customSliceAllocation <= pizzaslices)
        {
          pizzaslices -= customSliceAllocation;
          System.out.printf("%n%npizzaslices - %d%n%n", customSliceAllocation);
          player.getCombatPack().setAttack(player.getAttack() + customSliceAllocation);
        }else
          System.out.printf("%n%nNot Enough Pizza! Pizza! Pizza!%n%n%n");
    }
    ////////////////////////////////////////////////////////////////////////
    else if(shieldOn == false)
        System.out.printf("%n%nYou're out of pizzaslices ! Vest Available !");
  }

  // Implementation For The `class Character` raiseShield() Method.
  public void customVest(Character player)
  {
    if (player.getChi() > 0)
    {
      ++toggle;
      if(toggle == 1)
      {
        player.raiseShield();
        shieldOn = true;
      }else
        System.out.printf("%n%nShield is on%n", "");
    }else
      System.out.printf("%n!Chi is Empty!%n%n");
  }
}