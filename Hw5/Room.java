public class Room{

  // Priv. Instance Variable(s).
  private String roomName;
  private Room cursorRoom;
  private Room northRoom = null;
  private Room southRoom = null;
  private Room eastRoom = null;
  private Room westRoom = null;
  private String station_E ="SPIKED WALL";
  private String station_S ="SPIKED WALL";
  private String station_W ="SPIKED WALL";
  private String station_N ="SPIKED WALL";
  private int tally;

  // Priv. Static Variable(s)
  private static Room wall;

  // 1-Arg String Constructor.
  Room(String roomName)
  {
    cursorRoom = this;
    cursorRoom.roomName = roomName;
  }

  // Sets Wall
  public void setWall(Room room){ this.wall = room; }

  // Returns Wall
  public Room getWall(){ return wall; }

  // Sets Room Exits.
  public void setRoomExits(Room northRoom, Room southRoom, Room eastRoom, Room westRoom)
  {
    cursorRoom.southRoom = southRoom;
    cursorRoom.westRoom = westRoom;
    cursorRoom.eastRoom = eastRoom;
    cursorRoom.northRoom = northRoom;

    if(!(northRoom.equals(wall))){
      northRoom.southRoom = cursorRoom;
    } 
    if(!(eastRoom.equals(wall))){
      eastRoom.westRoom = cursorRoom;
    }
    if(!(southRoom.equals(wall))){
      southRoom.northRoom = cursorRoom;
    }
    if(!(westRoom.equals(wall))){
      westRoom.eastRoom = cursorRoom;
    }
  }
    

  // GETTERS FOR DIRECTIONS AND ROOM DESCRIPTION.
  public Room getNorth()
  {
    if (!(cursorRoom.northRoom.equals(wall))){
      cursorRoom = cursorRoom.northRoom;
    }

    else
    {
      ++tally;
      System.out.printf("%nOUCH! THERE'S A WALL THERE ! %n%n");
      tally=0;
    }
      return cursorRoom;
  }

  public Room getSouth()
  {
    if (!(cursorRoom.southRoom.equals(wall))){
      cursorRoom = cursorRoom.southRoom;
    }

    else
    {
      ++tally;
      System.out.printf("%nOUCH! THERE'S A WALL THERE ! %n%n");
      tally=0;
    }
      return cursorRoom;
  }

  public Room getWest()
  {
    if (!(cursorRoom.westRoom.equals(wall))){
      cursorRoom = cursorRoom.westRoom;
    }

    else
    {
      ++tally;
      System.out.printf("%nOUCH! THERE'S A WALL THERE ! %n%n");
      tally=0;
    }
      return cursorRoom;
  }

  public Room getEast()
  {
    if (!(cursorRoom.eastRoom.equals(wall))){
      cursorRoom = cursorRoom.eastRoom;
    }

    else
    {
      ++tally;
      System.out.printf("%nOUCH! THERE'S A WALL THERE ! %n%n");
      tally=0;
    }
      return cursorRoom;
  }

  public void getRoomDescriptions()
  {
    if (!(cursorRoom.northRoom.equals(wall))){
      cursorRoom.station_N = cursorRoom.northRoom.roomName;
    }
    if (!(cursorRoom.southRoom.equals(wall))){
      cursorRoom.station_S = cursorRoom.southRoom.roomName;
    }
    if (!(cursorRoom.eastRoom.equals(wall))){
      cursorRoom.station_E = cursorRoom.eastRoom.roomName;
    }
    if (!(cursorRoom.westRoom.equals(wall))){
      cursorRoom.station_W = cursorRoom.westRoom.roomName;
    }
    System.out.printf("`Welcome To GameCourseB`%n%n");
    System.out.printf("%s%n%n", "__________________________________________");
    System.out.printf("You are currently in %s%n", cursorRoom.roomName);
    System.out.printf("%s%n%n", "__________________________________________");
    System.out.printf("%n  --Rooms--%n%n|North: %s|%n|South: %s|%n|East: %s|%n|West: %s|%n",
    cursorRoom.station_N, cursorRoom.station_S, cursorRoom.station_E, cursorRoom.station_W);
  }
} 