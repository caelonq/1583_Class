public class Dungeon{

  // Priv. Room Objects(s).
  private Room room = new Room("SPIKED WALL");
  private Room roomO15 = new Room("orange_room15");
  private Room roomBe6 = new Room("beige_room6");
  private Room roomY12 = new Room("yellow_room12");  
  private Room roomBe5 = new Room("beige_room5");
  private Room roomBe7 = new Room("beige_room7");
  private Room roomBe8 = new Room("beige_room8");
  private Room roomLg3 = new Room("liGreen_room3");
  private Room roomLg4 = new Room("liGreen_room4");
  private Room roomG14 = new Room("green_room14");
  private Room roomG13 = new Room("green_room13");
  private Room roomY11 = new Room("yellow_room11");
  private Room roomY10 = new Room("yellow_room10");
  private Room roomY9 = new Room("yellow_room9");
  private Room roomR1 = new Room("red_room");
  private Room roomB2 = new Room("blue_room2");

  // No-Arg Constructor.
  Dungeon(){
    //room = null; Optional for exclusive exception handling
    room.setWall(room);
    roomR1.setRoomExits(room, room, roomY9, roomB2);
    roomB2.setRoomExits(roomLg3, room, roomR1, room);
    roomY9.setRoomExits(room, room, roomY10, roomR1);
    roomY10.setRoomExits(roomG13, room, roomY11, roomY9);
    roomY11.setRoomExits(roomG14, room, roomY12, roomY10);
    roomY12.setRoomExits(room, room, room, roomY11);
    roomG13.setRoomExits(room, roomY10, roomG14, room);
    roomG14.setRoomExits(roomO15, roomY11, room, roomG13);
    roomO15.setRoomExits(room, roomG14, room, room);
    roomLg3.setRoomExits(roomBe6, roomB2, room, roomLg4);
    roomLg4.setRoomExits(roomBe5, room, roomLg3, room);
    roomBe6.setRoomExits(room, roomLg3, room, roomBe5);
    roomBe5.setRoomExits(room, roomLg4, roomBe6, roomBe7);
    roomBe7.setRoomExits(room, room, roomBe5, roomBe8);
    roomBe8.setRoomExits(room, room, roomBe7, room);
  }

  // Returns Starting Room.
  public Room getStartingRoom(){ return roomR1; }
}


