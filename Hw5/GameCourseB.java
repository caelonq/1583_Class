import java.util.Scanner;

public class GameCourseB extends Dungeon
{
  // Priv. Static Instance Variable(s).
	private static Scanner input = new Scanner(System.in);
	private static Dungeon dungeon = new Dungeon();
  private static String grabDirection;
  private static boolean game = false;
  private static boolean over = true;

  // Main.
  public static void main(String[] args) { gameCourse_B(); }

  //gameCourse method to get input from user and move in directions NORTH, EAST, WEST, and SOUTH.
	public static void gameCourse_B()
  {	
	  do{ 
       dungeonDirectionMenu();
		  }while(game != over);
  }

  // Method To Display Menu, Direction, and Placement.
  public static void dungeonDirectionMenu()
  {
    dungeon.getStartingRoom().getRoomDescriptions();
    System.out.printf(" %nEnter a Direction (N,S,E,W)%nN()-NORTH |%nS()-SOUTH |%nE()-EAST |%n"); 
    System.out.printf("W()-WEST |%n(Q)-Ends Course%n");
    grabDirection = input.nextLine();
    System.out.printf("%s%n%n", "__________________________________________");
        switch(grabDirection)
        {
          case "n":
          case "N":
          System.out.printf("%s%n%n","YOU ARE MOVING NORTH");
          dungeon.getStartingRoom().getNorth();
          break;
          case "s":
          case "S":
          System.out.printf("%s%n%n","YOU ARE MOVING SOUTH");
          dungeon.getStartingRoom().getSouth();
          break;
          case "e":
          case "E":
          System.out.printf("%s%n%n","YOU ARE MOVING EAST");
          dungeon.getStartingRoom().getEast();
          break;
          case "w":
          case "W":
          System.out.printf("%s%n%n","YOU ARE MOVING WEST");
          dungeon.getStartingRoom().getWest();
          break;
          case "q":
          case "Q":
          game = over;
          System.out.print("Course Ended");
          break;
          default:
          System.out.printf("%s%n%n","Undefined Entry ");
        }
  }
}


